#!/usr/bin/env python2
import asyncore, asynchat
import os, socket, string
import json
import re
from inbox import Inbox
from datetime import datetime, timedelta
import random

inbox = Inbox()

mail = {}

@inbox.collate
def handle(body, to, sender):
    print 'to list:', to
    for t in to:
        m = re.match('([a-z0-9A-Z\_]+)@.*$', t)
        if m:
            t = m.group(1)
            t = t.lower()
            print 'mail to', t
            if t not in mail:
                mail[t] = {}
            mail[t][datetime.now()] = {'to': to, 'sender': sender, 'body': body, 'time': datetime.utcnow().isoformat()}
        else:
            print 'bad t:', t
    for l in mail.values():
        for tm in [tm for tm in l.keys() if tm < datetime.now() - timedelta(0, 60*30)]:
            print 'clean of', tm
            del l[tm]

def process(cmd, arg):
    if cmd == 'mail':
        d = mail.get(arg, {})
        return {"result": [v for _,v in sorted(d.items(), reverse=True)]}
    elif cmd == 'name':
        arg = arg.lower()
        if arg not in mail:
            mail[arg] = {}
            return {'result': arg}
        else:
            for i in xrange(1, 10000):
                k = arg + str(i)
                if k not in mail:
                    mail[k] = {}
                    return {"result": k}
            while k in mail:
                k = arg + str(random.randint(1, 100000000000000))
            mail[k] = {}
            return {"result": k}
    else:
        return None

class HTTPChannel(asynchat.async_chat):

    def __init__(self, server, sock, addr):
        asynchat.async_chat.__init__(self, sock)
        self.set_terminator("\r\n")
        self.request = None
        self.data = ""
        self.shutdown = 0

    def collect_incoming_data(self, data):
        self.data = self.data + data

    def found_terminator(self):
        if not self.request:
            # got the request line
            self.request = string.split(self.data, None, 2)
            if len(self.request) != 3 or not self.request[1].startswith('/'):
                self.shutdown = 1
            else:
                self.push("HTTP/1.0 200 OK\r\n")
                self.push("Content-type: application/json\r\n")
                self.push("\r\n")
            self.data = self.data + "\r\n"
            self.set_terminator("\r\n\r\n") # look for end of headers
        else:
            # return payload.
            path = self.request[1][1:]
            cmd, _, arg = path.partition('/')
            self.push(json.dumps(process(cmd, arg)))
            self.close_when_done()
            print 'served request', self.request[1][1:]

class HTTPServer(asyncore.dispatcher):

    def __init__(self, port):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.bind(("0.0.0.0", port))
        self.listen(5)

    def handle_accept(self):
        conn, addr = self.accept()
        HTTPChannel(self, conn, addr)

#
# try it out
PORT=8283
s = HTTPServer(PORT)
print "serving at port", PORT, "..."


inbox.serve(address='0.0.0.0', port=25)

