#!/usr/bin/env python2
"""orchmail.py
This module provides an interface to our private mail server
Example usage:


import orchmail

name = "john"
# request a unique username:
username = orchmail.username(name)
email = "%s@%s" % (username, orchmail.domain)
### now initiate sending a mail to generated email

# wait for incoming email:
try:
    for msg in orchmail.wait(username):
        print msg.body, msg.sender, msg.time, msg.to
except orchmail.Timeout:
    print "Mail delivery failed"

"""

import sys
import urllib
import urllib2
import contextlib
import json
import email
from datetime import datetime, timedelta
from collections import namedtuple
import iso8601
import time
import logging

logger = logging.getLogger('orchmail')

domain = 'orchidfest.ru'

url = 'http://%s:8283/' % domain

Msg = namedtuple('Msg', 'body sender time to')

class Timeout(Exception):
    pass

def username(name):
    "request unique username for given name"
    logger.debug('username request for "%s"', name)
    with contextlib.closing(urllib2.urlopen(url + 'name/' + name)) as f:
        s = f.read()
    t = json.loads(s)
    res = t['result']
    logger.debug('got username "%s" -> "%s"', name, res)
    return res

def fetch(username):
    """fetch inbox for given username
    returns a list of Msg objects

    example:

    for msg in fetch('john'):
        print msg.body, msg.sender, msg.time, msg.to
    """

    logger.debug('fetch mail for "%s"', username)
    with contextlib.closing(urllib2.urlopen(url + 'mail/' + username)) as f:
        s = f.read()
    t = json.loads(s)
    res = []
    for x in t['result']:
        res.append(Msg(x['body'], x['sender'], iso8601.parse_date(x['time']), x['to']))
    logger.debug("fetch mail - got %s messages", len(res))
    return res

def wait(username, timeout = 60 * 10):
    """wait for a letter for given username
    returns a list of Msg objects

    example:

    for msg in wait('john'):
        print msg.body, msg.sender, msg.time, msg.to
    """

    logger.info('wait mail for "%s"', username)
    res = fetch(username)
    tm = datetime.now()
    while not res and tm + timedelta(0, timeout) > datetime.now():
        logger.debug("sleep..")
        time.sleep(2)
        res = fetch(username)
    if not res:
        logger.warning("fetch mail - timeout %s secs exceeded", timeout)
        raise Timeout()
    return res

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    for msg in wait(sys.argv[1]):
        print msg.body, msg.sender, msg.time, msg.to  
